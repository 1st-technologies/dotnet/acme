# ACME

*System Center Configuration Manager - App Deployment and Support Console*

ACME is a simplified front-end console for SCCM to allow first level technicians to perform application installation and remote support without need for or complexity of the full SCCM console.

![sample](sample.png)

## Features

* View and edit membership of user or device collections
* View and edit primary user associations
* Launch SCCM Remote Console
