﻿Imports System.Management

Public Class Login
    ' TODO: Insert code to perform custom authentication using the provided username and password 
    ' (See http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Label1.Text = ""
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim objConnectionOptions As ConnectionOptions
        Dim secPassword As New Security.SecureString()

        objConnectionOptions = New ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = UsernameTextBox.Text
        objConnectionOptions.Password = PasswordTextBox.Text
        Dim objScope As New ManagementScope("\\" & My.Settings.SiteServer & "\root\sms", objConnectionOptions)

        Try
            objScope.Connect()
            Me.Close()
            Common.subWriteLog(Main.strLogFile, "Information", UsernameTextBox.Text, "Logged in")
        Catch ex As Exception
            Label1.Text = "Bad username or password"
            Common.subWriteLog(Main.strLogFile, "Warning", UsernameTextBox.Text, "Bad password or username")
        End Try

        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Application.Exit()
    End Sub
End Class
