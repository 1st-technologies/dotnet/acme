﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Options
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabGeneral = New System.Windows.Forms.TabPage()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.tabSCCM = New System.Windows.Forms.TabPage()
        Me.txtWorkstationCollection = New System.Windows.Forms.TextBox()
        Me.txtSiteCode = New System.Windows.Forms.TextBox()
        Me.txtSiteServer = New System.Windows.Forms.TextBox()
        Me.txtUserCollection = New System.Windows.Forms.TextBox()
        Me.tabDomain = New System.Windows.Forms.TabPage()
        Me.txtDomainName = New System.Windows.Forms.TextBox()
        Me.txtDomainLDAP = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        Me.tabSCCM.SuspendLayout()
        Me.tabDomain.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnApply, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(154, 306)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(227, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(4, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'btnApply
        '
        Me.btnApply.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnApply.Location = New System.Drawing.Point(155, 3)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(67, 23)
        Me.btnApply.TabIndex = 2
        Me.btnApply.Text = "Apply"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(79, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 35)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 13)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "Global Catalog"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Domain Name"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 157)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(138, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Workstation Collection Filter"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(45, 61)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "User Collection Filter"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(89, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Site Server"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(95, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Site Code"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabGeneral)
        Me.TabControl1.Controls.Add(Me.tabSCCM)
        Me.TabControl1.Controls.Add(Me.tabDomain)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(367, 281)
        Me.TabControl1.TabIndex = 3
        '
        'tabGeneral
        '
        Me.tabGeneral.Controls.Add(Me.TextBox1)
        Me.tabGeneral.Controls.Add(Me.Label2)
        Me.tabGeneral.Controls.Add(Me.CheckBox1)
        Me.tabGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGeneral.Size = New System.Drawing.Size(359, 255)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "General Settings"
        Me.tabGeneral.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "LOGDIR", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.TextBox1.Location = New System.Drawing.Point(93, 39)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(260, 20)
        Me.TextBox1.TabIndex = 18
        Me.TextBox1.Text = Global.ACME.My.MySettings.Default.LOGDIR
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Log Destination"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = Global.ACME.My.MySettings.Default.SHOWSTATUSBARONLOAD
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Global.ACME.My.MySettings.Default, "SHOWSTATUSBARONLOAD", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.CheckBox1.Location = New System.Drawing.Point(9, 16)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(137, 17)
        Me.CheckBox1.TabIndex = 17
        Me.CheckBox1.Text = "Show statusbar on load"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'tabSCCM
        '
        Me.tabSCCM.Controls.Add(Me.txtWorkstationCollection)
        Me.tabSCCM.Controls.Add(Me.txtSiteCode)
        Me.tabSCCM.Controls.Add(Me.txtSiteServer)
        Me.tabSCCM.Controls.Add(Me.txtUserCollection)
        Me.tabSCCM.Controls.Add(Me.Label3)
        Me.tabSCCM.Controls.Add(Me.Label4)
        Me.tabSCCM.Controls.Add(Me.Label7)
        Me.tabSCCM.Controls.Add(Me.Label6)
        Me.tabSCCM.Location = New System.Drawing.Point(4, 22)
        Me.tabSCCM.Name = "tabSCCM"
        Me.tabSCCM.Size = New System.Drawing.Size(359, 255)
        Me.tabSCCM.TabIndex = 2
        Me.tabSCCM.Text = "SCCM Settings"
        Me.tabSCCM.UseVisualStyleBackColor = True
        '
        'txtWorkstationCollection
        '
        Me.txtWorkstationCollection.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "WorkstationAppCollectionFilter", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtWorkstationCollection.Location = New System.Drawing.Point(154, 154)
        Me.txtWorkstationCollection.Multiline = True
        Me.txtWorkstationCollection.Name = "txtWorkstationCollection"
        Me.txtWorkstationCollection.Size = New System.Drawing.Size(200, 90)
        Me.txtWorkstationCollection.TabIndex = 5
        Me.txtWorkstationCollection.Text = Global.ACME.My.MySettings.Default.WorkstationAppCollectionFilter
        '
        'txtSiteCode
        '
        Me.txtSiteCode.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "SiteCode", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtSiteCode.Location = New System.Drawing.Point(154, 6)
        Me.txtSiteCode.Name = "txtSiteCode"
        Me.txtSiteCode.Size = New System.Drawing.Size(200, 20)
        Me.txtSiteCode.TabIndex = 1
        Me.txtSiteCode.Text = Global.ACME.My.MySettings.Default.SiteCode
        '
        'txtSiteServer
        '
        Me.txtSiteServer.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "SiteServer", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtSiteServer.Location = New System.Drawing.Point(154, 32)
        Me.txtSiteServer.Name = "txtSiteServer"
        Me.txtSiteServer.Size = New System.Drawing.Size(200, 20)
        Me.txtSiteServer.TabIndex = 2
        Me.txtSiteServer.Text = Global.ACME.My.MySettings.Default.SiteServer
        '
        'txtUserCollection
        '
        Me.txtUserCollection.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "UserAppCollectionFilter", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtUserCollection.Location = New System.Drawing.Point(154, 58)
        Me.txtUserCollection.Multiline = True
        Me.txtUserCollection.Name = "txtUserCollection"
        Me.txtUserCollection.Size = New System.Drawing.Size(200, 90)
        Me.txtUserCollection.TabIndex = 4
        Me.txtUserCollection.Text = Global.ACME.My.MySettings.Default.UserAppCollectionFilter
        '
        'tabDomain
        '
        Me.tabDomain.Controls.Add(Me.Label13)
        Me.tabDomain.Controls.Add(Me.Label8)
        Me.tabDomain.Controls.Add(Me.txtDomainName)
        Me.tabDomain.Controls.Add(Me.txtDomainLDAP)
        Me.tabDomain.Location = New System.Drawing.Point(4, 22)
        Me.tabDomain.Name = "tabDomain"
        Me.tabDomain.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDomain.Size = New System.Drawing.Size(359, 255)
        Me.tabDomain.TabIndex = 1
        Me.tabDomain.Text = "Domain Settings"
        Me.tabDomain.UseVisualStyleBackColor = True
        '
        'txtDomainName
        '
        Me.txtDomainName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "DOMAIN", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtDomainName.Location = New System.Drawing.Point(87, 6)
        Me.txtDomainName.Name = "txtDomainName"
        Me.txtDomainName.Size = New System.Drawing.Size(267, 20)
        Me.txtDomainName.TabIndex = 6
        Me.txtDomainName.Text = Global.ACME.My.MySettings.Default.DOMAIN
        '
        'txtDomainLDAP
        '
        Me.txtDomainLDAP.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ACME.My.MySettings.Default, "DOMAINPATH", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtDomainLDAP.Location = New System.Drawing.Point(88, 32)
        Me.txtDomainLDAP.Name = "txtDomainLDAP"
        Me.txtDomainLDAP.Size = New System.Drawing.Size(265, 20)
        Me.txtDomainLDAP.TabIndex = 11
        Me.txtDomainLDAP.Text = Global.ACME.My.MySettings.Default.DOMAINPATH
        '
        'Options
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(389, 343)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Options"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Options"
        Me.TopMost = True
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        Me.tabGeneral.PerformLayout()
        Me.tabSCCM.ResumeLayout(False)
        Me.tabSCCM.PerformLayout()
        Me.tabDomain.ResumeLayout(False)
        Me.tabDomain.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSiteCode As System.Windows.Forms.TextBox
    Friend WithEvents txtSiteServer As System.Windows.Forms.TextBox
    Friend WithEvents txtUserCollection As System.Windows.Forms.TextBox
    Friend WithEvents txtWorkstationCollection As System.Windows.Forms.TextBox
    Friend WithEvents txtDomainName As System.Windows.Forms.TextBox
    Friend WithEvents txtDomainLDAP As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabGeneral As System.Windows.Forms.TabPage
    Friend WithEvents tabDomain As System.Windows.Forms.TabPage
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents tabSCCM As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
