﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Collections
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgCollections = New System.Windows.Forms.DataGridView
        Me.dcgCollectionID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dcgCollectionName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dcgRemove = New System.Windows.Forms.DataGridViewLinkColumn
        Me.btnClose = New System.Windows.Forms.Button
        CType(Me.dgCollections, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgCollections
        '
        Me.dgCollections.AllowUserToAddRows = False
        Me.dgCollections.AllowUserToDeleteRows = False
        Me.dgCollections.AllowUserToResizeColumns = False
        Me.dgCollections.AllowUserToResizeRows = False
        Me.dgCollections.BackgroundColor = System.Drawing.Color.White
        Me.dgCollections.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgCollections.ColumnHeadersVisible = False
        Me.dgCollections.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dcgCollectionID, Me.dcgCollectionName, Me.dcgRemove})
        Me.dgCollections.Location = New System.Drawing.Point(12, 12)
        Me.dgCollections.MultiSelect = False
        Me.dgCollections.Name = "dgCollections"
        Me.dgCollections.ReadOnly = True
        Me.dgCollections.RowHeadersVisible = False
        Me.dgCollections.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgCollections.Size = New System.Drawing.Size(548, 279)
        Me.dgCollections.TabIndex = 0
        '
        'dcgCollectionID
        '
        Me.dcgCollectionID.HeaderText = "dcgCollectionID"
        Me.dcgCollectionID.Name = "dcgCollectionID"
        Me.dcgCollectionID.ReadOnly = True
        Me.dcgCollectionID.Visible = False
        '
        'dcgCollectionName
        '
        Me.dcgCollectionName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dcgCollectionName.HeaderText = "dcgCollectionName"
        Me.dcgCollectionName.Name = "dcgCollectionName"
        Me.dcgCollectionName.ReadOnly = True
        '
        'dcgRemove
        '
        Me.dcgRemove.HeaderText = "dcgRemove"
        Me.dcgRemove.Name = "dcgRemove"
        Me.dcgRemove.ReadOnly = True
        Me.dcgRemove.Text = "Remove"
        Me.dcgRemove.TrackVisitedState = False
        Me.dcgRemove.UseColumnTextForLinkValue = True
        Me.dcgRemove.Width = 55
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(249, 299)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Collections
        '
        Me.AcceptButton = Me.btnClose
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(572, 330)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.dgCollections)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Collections"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Collections"
        Me.TopMost = True
        CType(Me.dgCollections, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgCollections As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dcgCollectionID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dcgCollectionName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dcgRemove As System.Windows.Forms.DataGridViewLinkColumn
End Class
