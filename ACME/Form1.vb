﻿Imports System
Imports System.IO
Imports System.Management
Imports System.DirectoryServices
Imports Microsoft.ConfigurationManagement.ManagementProvider
Imports Microsoft.ConfigurationManagement.ManagementProvider.WqlQueryEngine

Public Class Main
    Dim dtCollections As New DataTable
    Dim strPreviousObjectCategory As String
    Public Shared strLogFile = My.Settings.LOGDIR & "\" & Now().Year.ToString & Now().Month.ToString("D2") & Now().Day.ToString("D2") & ".log"

    Private Sub TopWindow_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        My.Forms.Login.ShowDialog()

        ' set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        StatusStrip1.Visible = My.Settings.SHOWSTATUSBARONLOAD
        StatusBarToolStripMenuItem.Checked = My.Settings.SHOWSTATUSBARONLOAD

        ' DEBUG
        'Login.UsernameTextBox.Text = "ablanco1"
        'Login.PasswordTextBox.Text = ""
        'RemoteControlToolStripMenuItem.Enabled = True
        'RemoteControlToolStripMenuItem1.Enabled = True

        ' ** Input fncIfMember([group name], [login id])
        ' ** CSCCORP only, do specify domain in the login id field
        'Dim strUserID As String
        'If InStr(Login.UsernameTextBox.Text, "\") > 0 Then
        'strUserID = Split(Login.UsernameTextBox.Text, "\")(1)
        'Else
        'strUserID = Login.UsernameTextBox.Text
        'End If
        'MessageBox.Show(Common.fncIfMember("SCCM-HelpDesk", strUserID), "In Group?")

        If File.Exists(Directory.GetCurrentDirectory & "\AdminUI.WqlQueryEngine.dll") = False Then
            File.WriteAllBytes(Directory.GetCurrentDirectory & "\AdminUI.WqlQueryEngine.dll", My.Resources.AdminUI_WqlQueryEngine)
        End If

        If File.Exists(Directory.GetCurrentDirectory & "\Microsoft.ConfigurationManagement.ManagementProvider.dll") = False Then
            File.WriteAllBytes(Directory.GetCurrentDirectory & "\Microsoft.ConfigurationManagement.ManagementProvider.dll", My.Resources.Microsoft_ConfigurationManagement_ManagementProvider)
        End If

        dtCollections.Columns.Add("CollectionID")
        dtCollections.Columns.Add("Name")

        ' restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub subPopulateCollectionComboBox(ByVal strMask)
        ' clear combobox
        dtCollections.Clear()

        ' connect to WMI on SCCM server and enum collections
        Dim objConnectionOptions As ConnectionOptions
        objConnectionOptions = New ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New ManagementScope("\\" & My.Settings.SiteServer & "\root\sms\site_" & My.Settings.SiteCode, objConnectionOptions)
        objScope.Connect()
        Dim objQuery As New SelectQuery("SELECT * FROM SMS_Collection WHERE " & strMask)
        Dim objResults As New ManagementObjectSearcher(objScope, objQuery)
        Dim objItem As ManagementObject
        Dim strCollectionName As String

        ' Get wmi results into datatable
        For Each objItem In objResults.Get()
            Dim objDataRow As DataRow = dtCollections.NewRow
            strCollectionName = objItem.Properties.Item("Name").Value.ToString
            objDataRow("CollectionID") = objItem.Properties.Item("CollectionID").Value.ToString
            objDataRow("Name") = strCollectionName
            dtCollections.Rows.Add(objDataRow)
        Next

        ' copy datatable into combobox
        cmbCollections.DataBindings.Clear()
        Dim dvTemp As New DataView(dtCollections)
        dvTemp.Sort = "Name"
        Dim dt As New DataTable()
        dtCollections = dvTemp.ToTable()
        cmbCollections.DataSource = dtCollections
        cmbCollections.ValueMember = "CollectionID"
        cmbCollections.DisplayMember = "Name"
    End Sub

    Private Sub btnDeleteMember_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteMember.Click
        If cmbCollections.SelectedIndex >= 0 Then
            My.Forms.Remove.ShowDialog()
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ' set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        Dim bolRowFound As Boolean
        Dim strObject As String = Nothing
        Dim strObjectCategory As String = Nothing
        Dim intCollectionType As UInteger = Common.fncGetCollectionType(cmbCollections.SelectedValue, My.Settings.SiteServer, My.Settings.SiteCode)

        ' Get selected object
        Try
            strObject = dgResults.CurrentRow.Cells(1).Value.ToString
        Catch ex As Exception
        End Try

        Try
            strObject = dgAssociated.CurrentCell.Value.ToString
        Catch ex As Exception
        End Try

        ' Check if collection already selected
        bolRowFound = False
        For Each dgRow As DataGridViewRow In dgCollections.Rows
            If dgRow.Cells(0).Value = cmbCollections.SelectedValue And StrComp(dgRow.Cells(1).Value, strObject, CompareMethod.Text) = 0 Then
                bolRowFound = True
            End If
        Next

        ' Add ad obect and collection to batch list
        If bolRowFound = False And Len(strObject) > 0 Then
            If intCollectionType = 1 Then
                strObjectCategory = "user"
            ElseIf intCollectionType = 2 Then
                strObjectCategory = "device"
            End If
            Dim dgNewRow As String() = New String() {cmbCollections.SelectedValue, strObject, cmbCollections.Text, strObjectCategory}
            dgCollections.Rows.Add(dgNewRow)
        End If

        ' restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub dgCollections_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgCollections.CellClick
        If e.ColumnIndex = 4 Then
            dgCollections.Rows.Remove(dgCollections.Rows.Item(e.RowIndex))
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If dgCollections.RowCount > 0 Then
            Dim intReturn As Integer
            If MessageBox.Show("Are you sure that you wish to create the specified collection memberships?", "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                Dim intTotalTasks As Integer = dgCollections.Rows.Count
                For Each dgRow As DataGridViewRow In dgCollections.Rows
                    ToolStripProgressBar1.Increment(ToolStripProgressBar1.Maximum / intTotalTasks)
                    If StrComp(dgRow.Cells(3).Value, "device", CompareMethod.Text) = 0 Then
                        intReturn = Common.fncEditCollection(dgRow.Cells(0).Value, Common.fncGetResourceID(dgRow.Cells(1).Value, "computer", My.Settings.SiteServer, My.Settings.SiteCode), dgRow.Cells(1).Value, dgRow.Cells(3).Value, "add", My.Settings.SiteServer)
                        If intReturn = 0 Then
                            Common.subWriteLog(strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Added " & dgRow.Cells(1).Value & "' to '" & dgRow.Cells(2).Value & "'")
                        Else
                            MessageBox.Show("Error adding '" & dgRow.Cells(1).Value & "' to '" & dgRow.Cells(2).Value & "': " & intReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Common.subWriteLog(strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error adding " & dgRow.Cells(1).Value & "' to '" & dgRow.Cells(2).Value & "'")
                        End If
                    ElseIf StrComp(dgRow.Cells(3).Value, "user", CompareMethod.Text) = 0 Then
                        intReturn = Common.fncEditCollection(dgRow.Cells(0).Value, Common.fncGetResourceID(dgRow.Cells(1).Value, "person", My.Settings.SiteServer, My.Settings.SiteCode), dgRow.Cells(1).Value, dgRow.Cells(3).Value, "add", My.Settings.SiteServer)
                        If intReturn = 0 Then
                            Common.subWriteLog(strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Added " & dgRow.Cells(1).Value & "' to '" & dgRow.Cells(2).Value & "'")
                        Else
                            MessageBox.Show("Error adding '" & dgRow.Cells(1).Value & "' to '" & dgRow.Cells(2).Value & "': " & intReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Common.subWriteLog(strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error adding " & dgRow.Cells(1).Value & "' to '" & dgRow.Cells(2).Value & "'")
                        End If
                    End If
                Next
                MessageBox.Show("Collection update sucessfully submitted. It may take up to several minutes for new members to appear.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ToolStripProgressBar1.Value = 0
                Me.Cursor = Windows.Forms.Cursors.Default
            End If
        End If
    End Sub

    Private Sub txtADObject_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtADObject.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            ' set 'wait' cursor
            Me.Cursor = Windows.Forms.Cursors.WaitCursor

            ' disable View collection menuitem
            ViewCollectionsToolStripMenuItem.Enabled = False
            ViewCollectionsToolStripMenuItem1.Enabled = False

            ' set private variables
            Dim strDomain As String = Nothing
            Dim strDomainFQDN As String = Nothing
            Dim strUserID As String = Nothing
            Dim deTemp As New DirectoryEntry
            deTemp.Path = My.Settings.DOMAINPATH
            Dim dgNewRow As String() = Nothing



            'If UBound(Split(txtADObject.Text, "\", -1, CompareMethod.Text)) > 0 Then
            'strDomain = Split(txtADObject.Text, "\", -1, CompareMethod.Text)(0).ToString.ToUpper
            'strUserID = Split(txtADObject.Text, "\", -1, CompareMethod.Text)(1).ToString.ToUpper
            'Select Case Trim(UCase(strDomain))
            '    Case My.Settings.DOMAIN
            'deTemp.Path = My.Settings.DOMAINPATH
            '    Case My.Settings.RPHIDOMAIN
            'deTemp.Path = My.Settings.RPHIDOMAINPATH
            '    Case My.Settings.NEWSDAYDOMAIN
            'deTemp.Path = My.Settings.NEWSDAYDOMAINPATH
            '    Case Else
            'MessageBox.Show("If specifing a domain, Please use " & My.Settings.DOMAIN & ", " & My.Settings.RPHIDOMAIN & " or " & My.Settings.NEWSDAYDOMAIN & " domains.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            'Me.Cursor = Windows.Forms.Cursors.Default
            'Exit Sub
            'End Select
            'Else
            strUserID = txtADObject.Text
            'deTemp.Path = My.Settings.DOMAINPATH

            'End If

            ' clear search results and associated objects datagridviews
            dgResults.Rows.Clear()
            dgAssociated.Rows.Clear()

            deTemp.Username = Login.UsernameTextBox.Text
            deTemp.Password = Login.PasswordTextBox.Text
            deTemp.Path = My.Settings.DOMAINPATH

            Dim searcher As New DirectorySearcher(deTemp)
            searcher.PropertiesToLoad.Add("cn")
            searcher.PropertiesToLoad.Add("objectCategory")
            searcher.PropertiesToLoad.Add("displayName")
            searcher.PropertiesToLoad.Add("distinguishedName")
            searcher.PropertiesToLoad.Add("sAMAccountName")
            searcher.PropertiesToLoad.Add("UserPrincipalName")
            searcher.PropertiesToLoad.Add("CanonicalName")

            searcher.Filter = "(&(anr=" & strUserID & ")(| (objectClass=person)(objectClass=computer)))"
            Dim results As SearchResultCollection
            Try
                results = searcher.FindAll()
                For Each result In results
                    'For Each prop As DictionaryEntry In result.properties
                    ' MessageBox.Show(prop.Key.ToString)
                    'Next
                    If InStr(result.Properties("objectCategory")(0), "CN=Person,CN=Schema,CN=Configuration", CompareMethod.Text) > 0 Then
                        strDomainFQDN = Split(result.Properties("UserPrincipalName")(0), "@")(1)
                        strDomain = Common.GetNetbiosDomainName(strDomainFQDN)
                        'dgNewRow = New String() {result.Properties("distinguishedName")(0), strDomain & "\" & result.Properties("sAMAccountName")(0), strDomain & "\" & result.Properties("sAMAccountName")(0) & " (" & result.Properties("displayName")(0) & ")", "person"}
                        'dgNewRow = New String() {result.Properties("distinguishedName")(0), strDomain & "\" & result.Properties("sAMAccountName")(0), result.Properties("UserPrincipalName")(0) & " (" & result.Properties("displayName")(0) & ")", "person"}
                        dgNewRow = New String() {result.Properties("distinguishedName")(0), strDomain & "\" & result.Properties("sAMAccountName")(0), strDomain & "\" & result.Properties("sAMAccountName")(0) & " (" & result.Properties("displayName")(0) & ")", "person"}
                    ElseIf InStr(result.Properties("objectCategory")(0), "CN=Computer,CN=Schema,CN=Configuration", CompareMethod.Text) > 0 Then
                        strDomain = Split(result.Properties("CanonicalName")(0), "/")(0)
                        dgNewRow = New String() {result.Properties("distinguishedName")(0), result.Properties("cn")(0), result.Properties("cn")(0), "computer"}
                    End If

                    dgResults.Rows.Add(dgNewRow)
                Next
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            ' Clear default selection
            dgResults.ClearSelection()
            dgResults.CurrentCell = Nothing

            ' restore default cursor
            Me.Cursor = Windows.Forms.Cursors.Default
        End If
    End Sub

    Private Sub dgResults_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgResults.CellClick
        ' Set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        ' Enable View collection menuitem
        ViewCollectionsToolStripMenuItem.Enabled = True
        ViewCollectionsToolStripMenuItem1.Enabled = True

        ' Clear associated object datagridview
        dgAssociated.Rows.Clear()
        dgAssociated.ClearSelection()
        dgAssociated.CurrentCell = Nothing

        ' Private Variables
        Dim strObjectName As String = Nothing
        Dim strAssociatedCategory As String = Nothing
        Dim strQuery As String = Nothing

        ' conenct to WMI on SCCM server and enum associated devices
        Dim objConnectionOptions As ConnectionOptions
        objConnectionOptions = New ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New ManagementScope("\\" & My.Settings.SiteServer & "\root\sms\site_" & My.Settings.SiteCode)

        objScope.Connect()
        Dim strAccountName As String = dgResults.Rows(e.RowIndex).Cells(1).Value.ToString
        Dim strObjectCategory As String = dgResults.Rows(e.RowIndex).Cells(3).Value.ToString


        If StrComp(strObjectCategory, "person", CompareMethod.Text) = 0 Then
            'MessageBox.Show(strAccountName)
            btnDeleteMember.Image = My.Resources.user_delete
            RemoteControlToolStripMenuItem.Enabled = False
            RemoteControlToolStripMenuItem1.Enabled = False
            strQuery = ("SELECT ResourceName FROM SMS_UserMachineRelationship WHERE UniqueUserName = '" & strAccountName & "' AND IsActive = 1 AND Types IS NOT NULL")
        ElseIf StrComp(strObjectCategory, "computer", CompareMethod.Text) = 0 Then
            'MessageBox.Show(strAccountName)
            btnDeleteMember.Image = My.Resources.computer_delete
            RemoteControlToolStripMenuItem.Enabled = True
            RemoteControlToolStripMenuItem1.Enabled = True
            strQuery = ("SELECT UniqueUserName FROM SMS_UserMachineRelationship WHERE ResourceName='" & strAccountName & "' and IsActive='1' and Types IS NOT NULL")
        End If
        strQuery = Replace(strQuery, "\", "\\", 1, -1, CompareMethod.Text)

        Dim objQuery As New SelectQuery(strQuery)
        Dim objWMIResults As New ManagementObjectSearcher(objScope, objQuery)
        Dim objWMIItem As ManagementObject

        ' Build associated object list
        Try
            For Each objWMIItem In objWMIResults.Get()
                If StrComp(strObjectCategory, "person", CompareMethod.Text) = 0 Then
                    strObjectName = objWMIItem.Properties.Item("ResourceName").Value.ToString
                    strAssociatedCategory = "computer"
                ElseIf StrComp(strObjectCategory, "computer", CompareMethod.Text) = 0 Then
                    strObjectName = objWMIItem.Properties.Item("UniqueUserName").Value.ToString
                    strAssociatedCategory = "person"
                End If

                Dim dgNewRow As String() = New String() {strObjectName, strAssociatedCategory}
                dgAssociated.Rows.Add(dgNewRow)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try

        ' Clear default selection
        dgAssociated.ClearSelection()
        dgAssociated.CurrentCell = Nothing

        ' update the combobox if the object type has changed
        If StrComp(strObjectCategory, "computer", CompareMethod.Text) = 0 Then
            If StrComp(strPreviousObjectCategory, "computer", CompareMethod.Text) <> 0 Then
                subPopulateCollectionComboBox(My.Settings.WorkstationAppCollectionFilter)
            End If
            strPreviousObjectCategory = "computer"
        ElseIf StrComp(strObjectCategory, "person", CompareMethod.Text) = 0 Then
            If StrComp(strPreviousObjectCategory, "person", CompareMethod.Text) <> 0 Then
                subPopulateCollectionComboBox(My.Settings.UserAppCollectionFilter)
            End If
            strPreviousObjectCategory = "person"
        End If

        ' Restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub dgAssociated_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgAssociated.CellClick
        ' Set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        ' Enable View collection menuitem
        ViewCollectionsToolStripMenuItem.Enabled = True
        ViewCollectionsToolStripMenuItem1.Enabled = True

        dgResults.ClearSelection()
        dgResults.CurrentCell = Nothing

        ' Reload collection list
        Dim strObjectCategory As String = dgAssociated.Rows(e.RowIndex).Cells(1).Value.ToString

        ' update the combobox if the object type has changed
        If StrComp(strObjectCategory, "computer", CompareMethod.Text) = 0 Then
            RemoteControlToolStripMenuItem.Enabled = True
            RemoteControlToolStripMenuItem1.Enabled = True
            btnDeleteMember.Image = My.Resources.computer_delete
            If StrComp(strPreviousObjectCategory, "computer", CompareMethod.Text) <> 0 Then
                subPopulateCollectionComboBox(My.Settings.WorkstationAppCollectionFilter)
            End If
            strPreviousObjectCategory = "computer"
        ElseIf StrComp(strObjectCategory, "person", CompareMethod.Text) = 0 Then
            RemoteControlToolStripMenuItem.Enabled = False
            RemoteControlToolStripMenuItem1.Enabled = False
            btnDeleteMember.Image = My.Resources.user_delete
            If StrComp(strPreviousObjectCategory, "person", CompareMethod.Text) <> 0 Then
                subPopulateCollectionComboBox(My.Settings.UserAppCollectionFilter)
            End If
            strPreviousObjectCategory = "person"
        End If

        ' Restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusBarToolStripMenuItem.Click
        If StatusBarToolStripMenuItem.Checked = True Then
            StatusStrip1.Visible = True
        Else
            StatusStrip1.Visible = False
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        My.Forms.About.ShowDialog()
    End Sub

    Private Sub SetPrimaryUserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetPrimaryUserToolStripMenuItem.Click, SetPrimaryUserToolStripMenuItem1.Click
        My.Forms.PrimaryUser.ShowDialog()
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripMenuItem.Click
        txtADObject.Cut()
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem.Click
        txtADObject.Paste()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CutToolStripMenuItem.Click
        txtADObject.Cut()
    End Sub

    Private Sub UndoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UndoToolStripMenuItem.Click
        txtADObject.Undo()
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        dgCollections.Rows.Clear()
    End Sub

    Private Sub StartRemoteControlViewerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RemoteControlToolStripMenuItem.Click, RemoteControlToolStripMenuItem1.Click
        ' set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.Default

        Dim strLibFolder As String = "\00000409"
        Dim strTempFolder As String = Directory.GetCurrentDirectory()
        'Dim strTempFolder As String = "c:\work\temp"

        Dim strWorkstation As String = Nothing
        Dim strDomain As String = Nothing

        If UBound(Split(txtADObject.Text, "\", -1, CompareMethod.Text)) > 0 Then
            strDomain = Split(txtADObject.Text, "\", -1, CompareMethod.Text)(0).ToString.ToUpper
        Else
            strDomain = My.Settings.DOMAIN.ToUpper
        End If


        Directory.CreateDirectory(strTempFolder & "\" & strLibFolder)
        File.WriteAllBytes(strTempFolder & "\CmRcViewer.exe", My.Resources.CmRcViewer)
        File.WriteAllBytes(strTempFolder & "\msvcr100.dll", My.Resources.msvcr100)
        File.WriteAllBytes(strTempFolder & "\RdpCoreSccm.dll", My.Resources.RdpCoreSccm)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\_statvw.dll", My.Resources._statvw)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\baserc.dll", My.Resources.baserc)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\climsgs.dll", My.Resources.climsgs)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\CmRcViewerRes.dll", My.Resources.CmRcViewerRes)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\CompMgrRes.dll", My.Resources.CompMgrRes)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\provmsgs.dll", My.Resources.provmsgs)
        File.WriteAllBytes(strTempFolder & strLibFolder & "\srvrsgs.dll", My.Resources.srvmsgs)

        Try
            strWorkstation = dgResults.CurrentRow.Cells(1).Value.ToString
        Catch ex As Exception
        End Try

        Try
            strWorkstation = dgAssociated.CurrentRow.Cells(0).Value.ToString
        Catch ex As Exception
        End Try

        'strWorkstation = "d5r54cz1"

        Common.subWriteLog(strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Started Remote Control session to " & strWorkstation.ToUpper)
        Try
            Dim myProcess As New Diagnostics.ProcessStartInfo
            'myProcess.Arguments = strWorkstation & " \\" & My.Settings.SiteServer
            'myProcess.Domain = strDomain
            'myProcess.FileName = "CmRcViewer.exe"
            'myProcess.FileName = "notepad.exe"
            'myProcess.LoadUserProfile = True
            'myProcess.Password = Common.fncConvertToSecureString(Login.PasswordTextBox.Text)
            'myProcess.UserName = Login.UsernameTextBox.Text
            'myProcess.UseShellExecute = True
            'myProcess.WindowStyle = ProcessWindowStyle.Normal
            'myProcess.WorkingDirectory = strTempFolder
            'myProcess.WorkingDirectory = "c:\windows"
            Process.Start(strTempFolder & "\CmRcViewer.exe", strWorkstation & " \\" & My.Settings.SiteServer)
            'Process.Start(myProcess)

        Catch ex As Exception
            MessageBox.Show("Generic Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try




        ' restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub txtADObject_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtADObject.MouseHover
        ToolStripStatusLabel1.Text = "Enter a user or computer name i.e. [DOMAIN\]value"
        StatusStrip1.Refresh()
    End Sub

    Private Sub dgResults_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgResults.MouseHover
        ToolStripStatusLabel1.Text = "Displays the results of the Active Directory search"
        StatusStrip1.Refresh()
    End Sub

    Private Sub dgAssociated_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgAssociated.MouseHover
        ToolStripStatusLabel1.Text = "Displays associated items in the SCCM inventory"
        StatusStrip1.Refresh()
    End Sub

    Private Sub cmbCollections_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCollections.MouseHover
        ToolStripStatusLabel1.Text = "Select an application collection"
        StatusStrip1.Refresh()
    End Sub

    Private Sub btnAdd_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.MouseHover
        ToolStripStatusLabel1.Text = "Click to add the selected collection to the task queue"
        StatusStrip1.Refresh()
    End Sub

    Private Sub btnOK_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.MouseHover
        ToolStripStatusLabel1.Text = "Click to create the collection memberships selected"
        StatusStrip1.Refresh()
    End Sub

    Private Sub dgCollections_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgCollections.MouseHover
        ToolStripStatusLabel1.Text = "The list of collection menberships to create"
        StatusStrip1.Refresh()
    End Sub

    Private Sub btnDeleteMember_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteMember.MouseHover
        ToolStripStatusLabel1.Text = "Remove members from the selected collection"
        StatusStrip1.Refresh()
    End Sub

    Private Sub all_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtADObject.MouseLeave, dgResults.MouseLeave, dgAssociated.MouseLeave, cmbCollections.MouseLeave, dgCollections.MouseLeave, btnAdd.MouseLeave, btnOK.MouseLeave, btnDeleteMember.MouseLeave
        ToolStripStatusLabel1.Text = ""
        StatusStrip1.Refresh()
    End Sub

    Private Sub OpToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpToolStripMenuItem.Click
        My.Forms.Options.ShowDialog()
    End Sub

    Private Sub ViewCollectionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewCollectionsToolStripMenuItem.Click, ViewCollectionsToolStripMenuItem1.Click
        My.Forms.Collections.ShowDialog()
    End Sub

    Private Sub dgAssociated_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgAssociated.MouseClick
        'If e.Button = Windows.Forms.MouseButtons.Right Then

        'End If
    End Sub
End Class