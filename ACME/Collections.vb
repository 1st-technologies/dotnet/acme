﻿Imports System.Management

Public Class Collections


    Private Sub Collections_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strObject As String = Nothing
        Dim strObjectType As String = Nothing

        Try
            strObject = Main.dgResults.CurrentRow.Cells(1).Value.ToString
            strObjectType = Main.dgResults.CurrentRow.Cells(3).Value.ToString
        Catch ex As Exception
        End Try

        Try
            strObject = Main.dgAssociated.CurrentRow.Cells(0).Value.ToString
            strObjectType = Main.dgAssociated.CurrentRow.Cells(1).Value.ToString
        Catch ex As Exception
        End Try

        Me.Text = strObject
        Dim strResourceID As String = Common.fncGetResourceID(strObject, strObjectType, My.Settings.SiteServer, My.Settings.SiteCode)

        subGetCollections(strResourceID)
    End Sub

    Private Sub subGetCollections(ByVal strResourceID)
        ' Set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        ' Set private Variables
        Dim strCollectionID As String = My.Forms.Main.cmbCollections.SelectedValue
        Dim strCollectionName As String = My.Forms.Main.cmbCollections.Text
        Dim strQueryC As String = Nothing

        ' Clear associated object datagridview
        dgCollections.Rows.Clear()
        dgCollections.ClearSelection()
        dgCollections.CurrentCell = Nothing


        ' conenct to WMI on SCCM server and enum associated devices
        Dim objConnectionOptions As ConnectionOptions
        objConnectionOptions = New ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New ManagementScope("\\" & My.Settings.SiteServer & "\root\sms\site_" & My.Settings.SiteCode)

        ' Build associated object list
        Try
            objScope.Connect()


            Dim objQueryFCM As New SelectQuery("SELECT CollectionID FROM SMS_FullCollectionMembership WHERE ResourceID = '" & strResourceID & "' AND IsDirect = 'TRUE'")
            Dim objWMIResultsFCM As New ManagementObjectSearcher(objScope, objQueryFCM)
            Dim objWMIItemFCM As ManagementObject
            Dim objWMIItemC As ManagementObject

            For Each objWMIItemFCM In objWMIResultsFCM.Get()
                Dim objQueryC As New SelectQuery("SELECT Name FROM SMS_Collection WHERE CollectionID = '" & objWMIItemFCM.Properties.Item("CollectionID").Value.ToString & "' ORDER BY Name")
                Dim objWMIResultsC As New ManagementObjectSearcher(objScope, objQueryC)
                For Each objWMIItemC In objWMIResultsC.Get()
                    Dim dgNewRow As String() = New String() {objWMIItemFCM.Properties.Item("CollectionID").Value.ToString, objWMIItemC.Properties.Item("Name").Value.ToString}
                    dgCollections.Rows.Add(dgNewRow)
                Next
            Next
        Catch em As ManagementException
            MessageBox.Show(em.Message, "WMI Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Generic Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
        


        ' Clear default selection
        dgCollections.ClearSelection()
        dgCollections.CurrentCell = Nothing

        ' Restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub dgCollections_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgCollections.CellClick
        If e.ColumnIndex = 2 Then
            Dim strObject As String = Nothing
            Dim strObjectType As String = Nothing
            Dim strCollectionID As String = dgCollections.Rows.Item(e.RowIndex).Cells(0).Value.ToString
            Dim strCollectionName As String = dgCollections.Rows.Item(e.RowIndex).Cells(1).Value.ToString
            Dim intReturn As Integer = Nothing

            Try
                strObject = Main.dgResults.CurrentRow.Cells(1).Value.ToString
                strObjectType = Main.dgResults.CurrentRow.Cells(3).Value.ToString
            Catch ex As Exception
            End Try

            Try
                strObject = Main.dgAssociated.CurrentRow.Cells(0).Value.ToString
                strObjectType = Main.dgAssociated.CurrentRow.Cells(1).Value.ToString
            Catch ex As Exception
            End Try

            Me.Text = strObject
            Dim strResourceID As String = Common.fncGetResourceID(strObject, strObjectType, My.Settings.SiteServer, My.Settings.SiteCode)



            If MessageBox.Show("Are you sure that you want to remove '" & strObject & "' from '" & strCollectionName & "'?", "Confirm?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                If strObjectType = "person" Then
                    intReturn = Common.fncEditCollection(strCollectionID, strResourceID, strObject, "user", "remove", My.Settings.SiteServer)
                    If intReturn = 0 Then
                        MessageBox.Show("Collection update sucessfully submitted. It may take up to several minutes for members to be removed.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Common.subWriteLog(Main.strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Removed " & strObject & "' from '" & strCollectionName & "'")
                    Else
                        MessageBox.Show("Error removing '" & strObject & "' from '" & strCollectionName & "': " & intReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        Common.subWriteLog(Main.strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error removing " & strObject & "' from '" & strCollectionName & "'")
                    End If
                ElseIf strObjectType = "computer" Then
                    intReturn = Common.fncEditCollection(strCollectionID, strResourceID, strObject, "device", "remove", My.Settings.SiteServer)
                    If intReturn = 0 Then
                        MessageBox.Show("Collection update sucessfully submitted. It may take up to several minutes for members to be removed.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Common.subWriteLog(Main.strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Removed " & strObject & "' from '" & strCollectionName & "'")
                    Else
                        MessageBox.Show("Error removing '" & strObject & "' from '" & strCollectionName & "': " & intReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        Common.subWriteLog(Main.strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error removing " & strObject & "' from '" & strCollectionName & "'")
                    End If
                End If
                Me.Cursor = Windows.Forms.Cursors.Default
            End If
        End If
    End Sub
End Class