﻿Imports System
Imports System.IO
Imports System.DirectoryServices
Imports System.Management
Imports Microsoft.ConfigurationManagement.ManagementProvider
Imports Microsoft.ConfigurationManagement.ManagementProvider.WqlQueryEngine

Public Class Common
    Public Shared Function fncGetResourceID(ByVal strResourceName As String, ByVal strMode As String, ByVal strSiteServer As String, ByVal strSiteCode As String) As String
        Dim strQuery As String = Nothing
        Dim strReturn As String = Nothing

        ' connect to WMI on SCCM server and enum collections
        Dim objConnectionOptions As Management.ConnectionOptions
        objConnectionOptions = New Management.ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New Management.ManagementScope("\\" & strSiteServer & "\root\sms\site_" & strSiteCode, objConnectionOptions)
        objScope.Connect()
        strResourceName = Replace(strResourceName, "\", "\\", 1, -1, CompareMethod.Text)

        If StrComp(strMode, "person", CompareMethod.Text) = 0 Then
            strQuery = "SELECT ResourceID FROM SMS_R_USER WHERE UniqueUserName = '" & strResourceName & "'"
        ElseIf StrComp(strMode, "computer", CompareMethod.Text) = 0 Then
            strQuery = "SELECT ResourceID FROM SMS_R_SYSTEM WHERE Name = '" & strResourceName & "'"
        End If

        Try
            Dim objQuery As New Management.SelectQuery(strQuery)
            Dim objResults As New Management.ManagementObjectSearcher(objScope, objQuery)
            Dim objItem As Management.ManagementObject

            ' Get wmi results into datatable
            For Each objItem In objResults.Get()
                strReturn = objItem.Properties.Item("ResourceID").Value.ToString
            Next

        Catch ea As AccessViolationException
            MessageBox.Show(ea.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch em As ManagementException
            MessageBox.Show(em.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        fncGetResourceID = strReturn
    End Function

    Public Shared Sub subWriteLog(ByVal strLogFile As String, ByVal strErrorLevel As String, ByVal strUser As String, ByVal strMessage As String)
        Dim objLogFile As StreamWriter
        If File.Exists(strLogFile) = False Then
            objLogFile = File.CreateText(strLogFile)
        Else
            objLogFile = File.AppendText(strLogFile)
        End If
        objLogFile.WriteLine(Now() & vbTab & strErrorLevel & vbTab & strUser & vbTab & strMessage)
        objLogFile.Close()
        objLogFile = Nothing
    End Sub

    Public Shared Function fncGetCollectionType(ByVal strCollectionID As String, ByVal strSiteServer As String, ByVal strSiteCode As String) As String
        Dim intReturn As UInteger

        ' connect to WMI on SCCM server and enum collections
        Dim objConnectionOptions As Management.ConnectionOptions
        objConnectionOptions = New Management.ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New Management.ManagementScope("\\" & strSiteServer & "\root\sms\site_" & strSiteCode, objConnectionOptions)


        Try
            objScope.Connect()
            Dim objQuery As New Management.SelectQuery("SELECT CollectionType FROM SMS_Collection WHERE CollectionID = '" & strCollectionID & "'")
            Dim objResults As New Management.ManagementObjectSearcher(objScope, objQuery)
            Dim objItem As Management.ManagementObject

            ' Get wmi results into datatable
            For Each objItem In objResults.Get()
                intReturn = objItem.Properties.Item("CollectionType").Value.ToString
            Next

        Catch ea As AccessViolationException
            MessageBox.Show(ea.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch em As ManagementException
            MessageBox.Show(em.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        fncGetCollectionType = intReturn
    End Function

    Public Shared Function fncEditCollection(ByVal CollectionID As String, ByVal strResourceId As String, ByVal ObjectName As String, ByVal strCollectionType As String, ByVal strMode As String, ByVal strSiteServer As String) As Integer
        Dim intReturn As Integer = Nothing
        Dim connection As New WqlConnectionManager
        Dim strResourceClassName As String
        Dim strMethod As String = Nothing

        If StrComp(strMode, "add", CompareMethod.Text) = 0 Then
            strMethod = "AddMembershipRule"
        ElseIf StrComp(strMode, "remove", CompareMethod.Text) = 0 Then
            strMethod = "DeleteMembershipRule"
        End If

        Try
            connection.Connect(strSiteServer, Login.UsernameTextBox.Text, Login.PasswordTextBox.Text)

            Dim oNewRule As IResultObject = connection.CreateEmbeddedObjectInstance("SMS_CollectionRuleDirect")
            If StrComp(strCollectionType, "device", CompareMethod.Text) = 0 Then
                strResourceClassName = "SMS_R_System"
            ElseIf StrComp(strCollectionType, "user", CompareMethod.Text) = 0 Then
                strResourceClassName = "SMS_R_User"
            Else
                strResourceClassName = "SMS_R_System"
            End If

            oNewRule("ResourceClassName").StringValue = strResourceClassName
            oNewRule("RuleName").StringValue = ObjectName
            oNewRule("ResourceID").IntegerValue = strResourceId

            Dim inParams As Dictionary(Of String, Object) = New Dictionary(Of String, Object)
            inParams.Add("collectionRule", oNewRule)

            Dim oCollection As IResultObject = connection.GetInstance("SMS_Collection.CollectionID='" & CollectionID & "'")
            Dim staticID As IResultObject = oCollection.ExecuteMethod(strMethod, inParams)
            intReturn = staticID("ReturnValue").IntegerValue

        Catch es As SmsException
            MessageBox.Show(es.Message, "SCCM Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ea As AccessViolationException
            MessageBox.Show(ea.Message, "Access Violation Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch em As ManagementException
            MessageBox.Show(em.Message, "WMI Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Generic Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Return intReturn
    End Function

    Public Shared Function fncIfMember(ByVal strGroupName As String, ByVal strUserName As String, ByVal strLoginID As String, ByVal strPassword As String) As Boolean
        Dim deTemp As New DirectoryEntry
        Dim bolReturn As Boolean = False

        deTemp.Path = My.Settings.DOMAINPATH
        deTemp.Username = strLoginID
        deTemp.Password = strPassword

        Dim searcher As New DirectorySearcher(deTemp)
        searcher.PropertiesToLoad.Add("cn")
        searcher.PropertiesToLoad.Add("member")

        searcher.Filter = "(&(cn=" & strGroupName & ")(objectClass=group))"
        Dim results As SearchResultCollection

        Try
            results = searcher.FindAll()
            For Each result In results
                For Each strMember As String In result.Properties("member")
                    If StrComp(fncGetsAMAccountName(strMember), strUserName, CompareMethod.Text) = 0 Then
                        bolReturn = True
                    End If
                Next
            Next
        Catch ead As DirectoryServicesCOMException
            MessageBox.Show(ead.Message, "AD Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Generic Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        fncIfMember = bolReturn
    End Function


    Public Shared Function fncGetsAMAccountName(ByVal strdistinguishedName) As String
        Dim deTemp As New DirectoryEntry
        Dim strReturn As String = Nothing

        deTemp.Path = My.Settings.DOMAINPATH
        deTemp.Username = Login.UsernameTextBox.Text
        deTemp.Password = Login.PasswordTextBox.Text

        Dim searcher As New DirectorySearcher(deTemp)
        searcher.PropertiesToLoad.Add("sAMAccountName")
        searcher.PropertiesToLoad.Add("distinguishedName")

        searcher.Filter = "(distinguishedName=" & strdistinguishedName & ")"
        Dim results As SearchResultCollection

        Try
            results = searcher.FindAll()
            For Each result In results
                strReturn = result.Properties("sAMAccountName")(0)
            Next
        Catch ead As DirectoryServicesCOMException
            MessageBox.Show(ead.Message, "AD Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Generic Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        fncGetsAMAccountName = Trim(strReturn)
    End Function

    Public Shared Function GetNetbiosDomainName(ByVal dnsDomainName As String) As String
        Dim netbiosDomainName As String = String.Empty
        Dim rootDSE As DirectoryEntry = New DirectoryEntry(String.Format("LDAP://{0}/RootDSE", dnsDomainName))
        Dim configurationNamingContext As String = rootDSE.Properties("configurationNamingContext")(0).ToString()
        Dim searchRoot As DirectoryEntry = New DirectoryEntry("LDAP://cn=Partitions," & configurationNamingContext)
        Dim searcher As DirectorySearcher = New DirectorySearcher(searchRoot)
        searcher.SearchScope = SearchScope.OneLevel
        searcher.PropertiesToLoad.Add("netbiosname")
        searcher.Filter = String.Format("(&(objectcategory=Crossref)(dnsRoot={0})(netBIOSName=*))", dnsDomainName)
        Dim result As SearchResult = searcher.FindOne()

        If result IsNot Nothing Then
            netbiosDomainName = result.Properties("netbiosname")(0).ToString()
        End If

        Return netbiosDomainName
    End Function

    Public Shared Function fncConvertToSecureString(ByVal strIn) As Security.SecureString
        Dim secOut As New Security.SecureString()
        'If strIn Is Nothing Then
        'Throw New ArgumentNullException("password")
        'End If

        For Each c As Char In strIn
            secOut.AppendChar(c)
        Next
        secOut.MakeReadOnly()
        Return secOut
    End Function
End Class
