﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Remove
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgMembers = New System.Windows.Forms.DataGridView
        Me.dcgResourceID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dcgType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dcgRemove = New System.Windows.Forms.DataGridViewLinkColumn
        Me.btnClose = New System.Windows.Forms.Button
        CType(Me.dgMembers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgMembers
        '
        Me.dgMembers.AllowUserToAddRows = False
        Me.dgMembers.AllowUserToDeleteRows = False
        Me.dgMembers.AllowUserToResizeColumns = False
        Me.dgMembers.AllowUserToResizeRows = False
        Me.dgMembers.BackgroundColor = System.Drawing.Color.White
        Me.dgMembers.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgMembers.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgMembers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgMembers.ColumnHeadersVisible = False
        Me.dgMembers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dcgResourceID, Me.dgcName, Me.dcgType, Me.dcgRemove})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgMembers.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgMembers.Location = New System.Drawing.Point(12, 12)
        Me.dgMembers.Name = "dgMembers"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgMembers.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgMembers.RowHeadersVisible = False
        Me.dgMembers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgMembers.Size = New System.Drawing.Size(313, 150)
        Me.dgMembers.TabIndex = 0
        '
        'dcgResourceID
        '
        Me.dcgResourceID.FillWeight = 89.0863!
        Me.dcgResourceID.HeaderText = "dcgResourceID"
        Me.dcgResourceID.Name = "dcgResourceID"
        Me.dcgResourceID.Visible = False
        Me.dcgResourceID.Width = 116
        '
        'dgcName
        '
        Me.dgcName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcName.FillWeight = 125.2635!
        Me.dgcName.HeaderText = "dgcName"
        Me.dgcName.Name = "dgcName"
        '
        'dcgType
        '
        Me.dcgType.HeaderText = "dcgType"
        Me.dcgType.Name = "dcgType"
        Me.dcgType.Visible = False
        '
        'dcgRemove
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dcgRemove.DefaultCellStyle = DataGridViewCellStyle2
        Me.dcgRemove.FillWeight = 85.65023!
        Me.dcgRemove.HeaderText = "dcgRemove"
        Me.dcgRemove.Name = "dcgRemove"
        Me.dcgRemove.Text = "Remove"
        Me.dcgRemove.TrackVisitedState = False
        Me.dcgRemove.UseColumnTextForLinkValue = True
        Me.dcgRemove.Width = 55
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(131, 184)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Remove
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(337, 224)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.dgMembers)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Remove"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Remove Members"
        CType(Me.dgMembers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgMembers As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dcgResourceID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dcgType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dcgRemove As System.Windows.Forms.DataGridViewLinkColumn
End Class
