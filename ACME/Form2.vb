﻿Imports System.Management
Imports Microsoft.ConfigurationManagement.ManagementProvider
Imports Microsoft.ConfigurationManagement.ManagementProvider.WqlQueryEngine

Public Class Remove

    Private Sub Remove_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = My.Forms.Main.cmbCollections.Text
        subGetCollectionMembers()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub subGetCollectionMembers()
        ' Set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        ' Set private Variables
        Dim strCollectionID = My.Forms.Main.cmbCollections.SelectedValue
        Dim strCollectionName = My.Forms.Main.cmbCollections.Text

        ' Clear associated object datagridview
        dgMembers.Rows.Clear()
        dgMembers.ClearSelection()
        dgMembers.CurrentCell = Nothing


        ' conenct to WMI on SCCM server and enum associated devices
        Dim objConnectionOptions As ConnectionOptions
        objConnectionOptions = New ConnectionOptions
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New ManagementScope("\\" & My.Settings.SiteServer & "\root\sms\site_" & My.Settings.SiteCode)
        objScope.Connect()


        Dim objQuery As New SelectQuery("SELECT ResourceID, ResourceType, Name FROM SMS_CM_RES_COLL_" & UCase(strCollectionID))
        Dim objWMIResults As New ManagementObjectSearcher(objScope, objQuery)
        Dim objWMIItem As ManagementObject

        ' Build associated object list
        For Each objWMIItem In objWMIResults.Get()
            Dim dgNewRow As String() = New String() {objWMIItem.Properties.Item("ResourceID").Value.ToString, objWMIItem.Properties.Item("Name").Value.ToString, objWMIItem.Properties.Item("ResourceType").Value.ToString}
            dgMembers.Rows.Add(dgNewRow)
        Next

        ' Clear default selection
        dgMembers.ClearSelection()
        dgMembers.CurrentCell = Nothing

        ' Restore default cursor
        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub dgMembers_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgMembers.CellClick
        Dim intReturn As Integer = Nothing

        If e.ColumnIndex = 3 Then
            Dim strResourceID As String = dgMembers.Rows.Item(e.RowIndex).Cells(0).Value.ToString
            Dim strResourceName As String = dgMembers.Rows.Item(e.RowIndex).Cells(1).Value.ToString
            Dim strResourceType As String = dgMembers.Rows.Item(e.RowIndex).Cells(2).Value.ToString
            Dim strCollectionName As String = My.Forms.Main.cmbCollections.Text
            Dim strCollectionID As String = My.Forms.Main.cmbCollections.SelectedValue


            If MessageBox.Show("Are you sure that you want to remove '" & strResourceName & "' from '" & strCollectionName & "'?", "Confirm?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                If strResourceType = 4 Then
                    intReturn = Common.fncEditCollection(strCollectionID, strResourceID, strResourceName, "user", "remove", My.Settings.SiteServer)
                    If intReturn = 0 Then
                        MessageBox.Show("Collection update sucessfully submitted. It may take up to several minutes for members to be removed.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Common.subWriteLog(Main.strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Removed " & strResourceName & "' from '" & strCollectionName & "'")
                    Else
                        MessageBox.Show("Error removing '" & strResourceName & "' from '" & strCollectionName & "': " & intReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        Common.subWriteLog(Main.strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error removing " & strResourceName & "' from '" & strCollectionName & "'")
                    End If
                ElseIf strResourceType = 5 Then
                    intReturn = Common.fncEditCollection(strCollectionID, strResourceID, strResourceName, "device", "remove", My.Settings.SiteServer)
                    If intReturn = 0 Then
                        MessageBox.Show("Collection update sucessfully submitted. It may take up to several minutes for members to be removed.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Common.subWriteLog(Main.strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Removed " & strResourceName & "' from '" & strCollectionName & "'")
                    Else
                        MessageBox.Show("Error removing '" & strResourceName & "' from '" & strCollectionName & "': " & intReturn, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        Common.subWriteLog(Main.strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error removing " & strResourceName & "' from '" & strCollectionName & "'")
                    End If
                End If
                Me.Cursor = Windows.Forms.Cursors.Default
                'subGetCollectionMembers()
            End If
        End If
    End Sub
End Class