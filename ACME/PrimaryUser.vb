﻿Imports System.Management
Imports Microsoft.ConfigurationManagement.ManagementProvider
Imports Microsoft.ConfigurationManagement.ManagementProvider.WqlQueryEngine

Public Class PrimaryUser
    Private Sub PrimaryUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strObject As String = Nothing
        Dim strObjectType As String = Nothing
        Try
            strObject = Main.dgResults.CurrentRow.Cells(1).Value.ToString
            strObjectType = Main.dgResults.CurrentRow.Cells(3).Value.ToString
        Catch ex As Exception
        End Try

        Try
            strObject = Main.dgAssociated.CurrentRow.Cells(0).Value.ToString
            strObjectType = Main.dgAssociated.CurrentRow.Cells(1).Value.ToString
        Catch ex As Exception
        End Try

        If StrComp(strObjectType, "person", CompareMethod.Text) = 0 Then
            txtUser.Text = strObject
            txtComputer.Text = ""
        ElseIf StrComp(strObjectType, "computer", CompareMethod.Text) = 0 Then
            txtUser.Text = ""
            txtComputer.Text = strObject
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' set 'wait' cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        If Len(txtComputer.Text) = 0 Or Len(txtUser.Text) = 0 Then
            MessageBox.Show("You must enter both a user and computer name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Else
            Dim intReturn As Integer = fncSetPrimaryUser(txtUser.Text, (Common.fncGetResourceID(txtComputer.Text, "computer", My.Settings.SiteServer, My.Settings.SiteCode)))
            If intReturn = 0 Then
                Common.subWriteLog(Main.strLogFile, "Information", My.Forms.Login.UsernameTextBox.Text, "Set primary user of  " & txtComputer.Text & " to " & txtUser.Text)
                MessageBox.Show("Primary user set sucessfully.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                Common.subWriteLog(Main.strLogFile, "Error", My.Forms.Login.UsernameTextBox.Text, "Error setting primary user of  " & txtComputer.Text & " to " & txtUser.Text & " (" & intReturn & ")")
                MessageBox.Show("There was an error setting the primary user.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
            Me.Close()
        End If

        ' Restore normal cursor
        Me.Cursor = Windows.Forms.Cursors.Default

    End Sub



    Private Function fncSetPrimaryUser(ByVal strUserID, ByVal strComputerID) As Integer
        ' connect to WMI on SCCM server and enum collections
        Dim objConnectionOptions As New ConnectionOptions()
        objConnectionOptions.Authentication = 6
        objConnectionOptions.Username = Login.UsernameTextBox.Text
        objConnectionOptions.Password = Login.PasswordTextBox.Text
        Dim objScope As New ManagementScope("\\" & My.Settings.SiteServer & "\root\sms\site_" & My.Settings.SiteCode, objConnectionOptions)
        Try
            objScope.Connect()
        Catch em As ManagementException
            MessageBox.Show(em.Message, "WMI Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Generic Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try

        Dim objPath As New ManagementPath("SMS_UserMachineRelationship")
        Dim objGetOptions As New ObjectGetOptions(Nothing, System.TimeSpan.MaxValue, True)


        Dim objWMIClass As New ManagementClass(objScope, objPath, objGetOptions)
        Dim inParams As ManagementBaseObject
        inParams = objWMIClass.GetMethodParameters("CreateRelationShip")
        inParams("MachineResourceID") = CUInt(strComputerID)
        inParams("SourceID") = CUInt(6)
        inParams("TypeID") = CUInt(1)
        inParams("UserAccountName") = strUserID

        Dim outParams As ManagementBaseObject
        Try
            outParams = objWMIClass.InvokeMethod("CreateRelationShip", inParams, Nothing)
            fncSetPrimaryUser = outParams("returnValue")
        Catch ex As Exception
            fncSetPrimaryUser = 1
        End Try
    End Function
End Class